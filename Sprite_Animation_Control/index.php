<!doctype html>
<html lang="en">
<head>
	<title>Sprite Animation Keyboard Controlled (Three.js)</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
</head>
<body>

<script src="libs/three.js"></script>
<script src="libs/Detector.js"></script>
<script src="libs/OrbitControls.js"></script>


<div id="ThreeJS" style="position: absolute; left:0px; top:0px"></div>
<script src="js/main.js"></script>

</body>
</html>


