/*-- ------------------------------------------------------------ 
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

*/

var camera1, camera2, scene, renderer;
var mesh, crates = [];
var pos = [];
var aspect = window.innerWidth / window.innerHeight 
var windowWidth, windowHeight;
var mouseX = 0, mouseY = 0;


var views = [
  {
    left: 0,
    top: 0,
    width: 0.5,
    height: 1.0,
    background: new THREE.Color( 0.5, 0.5, 0.7 ),
    eye: [ 0, 300, 1800 ],
    up: [ 0, 1, 0 ],
    fov: 30,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.x += mouseX * 0.05;
      camera.position.x = Math.max( Math.min( camera.position.x, 2000 ), -2000 );
      camera.lookAt( scene.position );
    }

  },
  {
    left: 0.5,
    top: 0.5,
    width: 0.5,
    height: 0.5,
    background: new THREE.Color( 0.7, 0.5, 0.5 ),
    eye: [ 0, 1800, 0 ],
    up: [ 0, 0, 1 ],
    fov: 45,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.x -= mouseX * 0.05;
      camera.position.x = Math.max( Math.min( camera.position.x, 2000 ), -2000 );
      camera.lookAt( camera.position.clone().setY( 0 ) );
    }

  },
  {
    left: 0.5,
    top: 0,
    width: 0.5,
    height: 0.5,
    background: new THREE.Color( 0.5, 0.7, 0.7 ),
    eye: [ 1400, 800, 1400 ],
    up: [ 0, 1, 0 ],
    fov: 60,
    updateCamera: function ( camera, scene, mouseX, mouseY ) {
      camera.position.y -= mouseX * 0.05;
      camera.position.y = Math.max( Math.min( camera.position.y, 1600 ), -1600 );
      camera.lookAt( scene.position );
    }
  }
];

init();
animate();

function init() {
  scene = new THREE.Scene();
  for (var ii =  0; ii < views.length; ++ii ) {

    var view = views[ii];
    camera = new THREE.PerspectiveCamera( view.fov, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.fromArray( view.eye );
    camera.up.fromArray( view.up );
    view.camera = camera;

  }

  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  //addControls();
  //

  onWindowResize();
  window.addEventListener( 'resize', onWindowResize, false );

  document.addEventListener( 'mousemove', onDocumentMouseMove, false );

  ADD = setInterval(addCube,50);
}

function addCube(){
  var grid = 5;
  var size = 50;
  var offset = (size * grid)/2;

  //check if all spots have been filled
  if(pos.length >= grid*grid*grid){
    crates.forEach(function(c){
      scene.remove(c);
      pos=[];
    });
    return false;
  }

  var texture = new THREE.TextureLoader().load( 'textures/crate.gif' );
  var geometry = new THREE.BoxBufferGeometry( size, size, size );
  var material = new THREE.MeshBasicMaterial( { map: texture } );

  mesh = new THREE.Mesh( geometry, material );
  mesh.size = size;

  var p = Math.floor((Math.random() * grid * grid * grid));

  //check if position has already been used
  if(pos.includes(p)){
    addCube();
  }else{
    pos.push(p);

    var row = (p%grid) *size;
    var vert = Math.floor((p%(grid*grid))/grid)*size;
    //console.log(p);
    var col = Math.floor(p/grid/grid)*size;
    //console.log(row + " : " + vert + " : " + col);
    mesh.position.set(row - offset,vert - offset,col - offset);
    crates.push(mesh);
    scene.add( mesh );
    return mesh;
  }
}

function onWindowResize() {

  if ( windowWidth != window.innerWidth || windowHeight != window.innerHeight ) {

    windowWidth  = window.innerWidth;
    windowHeight = window.innerHeight;

    renderer.setSize ( windowWidth, windowHeight );

  }

}

function animate() {
  requestAnimationFrame( animate );

  for ( var ii = 0; ii < views.length; ++ii ) {

    var view = views[ii];
    camera = view.camera;

    view.updateCamera( camera, scene, mouseX, mouseY );

    var left   = Math.floor( windowWidth  * view.left );
    var top    = Math.floor( windowHeight * view.top );
    var width  = Math.floor( windowWidth  * view.width );
    var height = Math.floor( windowHeight * view.height );

    renderer.setViewport( left, top, width, height );
    renderer.setScissor( left, top, width, height );
    renderer.setScissorTest( true );
    renderer.setClearColor( view.background );

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.render( scene, camera );

  }
}

function addControls(){
  // controls
  var controls = new THREE.OrbitControls( camera, renderer.domElement );
  controls.maxPolarAngle = Math.PI * 0.5;
  controls.minDistance = 100;
  controls.maxDistance = 1000;
}

function onDocumentMouseMove( event ) {

  mouseX = ( event.clientX - windowWidth / 2 );
  mouseY = ( event.clientY - windowHeight / 2 );

}
