/*-- ------------------------------------------------------------ 
Code By Kris Occhipinti 2018
http://filmsbykris.com

Based on example from:
Three.js "tutorials by example"
Author: Lee Stemkoski
Date: July 2013 (three.js v59dev)
http://stemkoski.github.io/Three.js/Texture-Animation.html
*/
// standard global variables
var container, scene, camera, renderer, controls;
var clock = new THREE.Clock();

// custom global variables
var animations = []; // animators
var doomguy;

init();
animate();

// FUNCTIONS 		
function init() 
{
  // SCENE
  scene = new THREE.Scene();

  createRenderer();
  createCamera();

  container = document.getElementById( 'ThreeJS' );
  container.appendChild( renderer.domElement );

  controls = new THREE.OrbitControls( camera, renderer.domElement );
  addLights();

  //create Doom Guy
  doomguy = createSprite('sprites/doomguy_walk.png');
  doomguy.scale.set(.5,.5,.5)
  doomguy.position.y=20;

  //clone Doom Guy
  doomguy2 = doomguy.clone();
  doomguy2.position.x+=50;
  scene.add(doomguy2);

  createGrid({size:200,step:10});

  //check for window resize
  window.addEventListener( 'resize', onWindowResize, false );
}

function createRenderer(){
  // RENDERER
  if ( Detector.webgl ){
    renderer = new THREE.WebGLRenderer( {antialias:true} );
  }else{
    renderer = new THREE.CanvasRenderer(); 
  }
  var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
  renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
}

function createCamera(){
  // CAMERA
  var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
  var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
  camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
  scene.add(camera);
  camera.position.set(0,150,400);
  camera.lookAt(scene.position);	

}

function addLights(){
  // LIGHT
  var light = new THREE.PointLight(0xffffff);
  light.position.set(0,250,0);
  scene.add(light);
}

function createSprite(img){
  // ANIMATION!
  var Texture = new THREE.ImageUtils.loadTexture( img );
  animations.push(new TextureAnimator( Texture, 6, 1, 6, 175 )); // texture, #horiz, #vert, #total, duration.
  var Material = new THREE.MeshBasicMaterial( { map: Texture, side:THREE.DoubleSide, transparent : true  } );
  var Geometry = new THREE.PlaneGeometry(60, 80, 1, 1);
  var sprite = new THREE.Mesh(Geometry, Material);
  scene.add(sprite);
  return sprite;

}

function animate() 
{
  var pos = new THREE.Vector3( camera.position.x, 0, camera.position.z );
  doomguy.lookAt(pos);
  requestAnimationFrame( animate );
  render();		
  update();
}

function update()
{
  animations.forEach(function(a){
    var delta = clock.getDelta(); 
    a.update(1000 * delta);
  });
  controls.update();
}

function render() 
{
  renderer.render( scene, camera );
}

function TextureAnimator(texture, tilesHoriz, tilesVert, numTiles, tileDispDuration) 
{	
  // note: texture passed by reference, will be updated by the update function.

  this.tilesHorizontal = tilesHoriz;
  this.tilesVertical = tilesVert;
  // how many images does this spritesheet contain?
  //  usually equals tilesHoriz * tilesVert, but not necessarily,
  //  if there at blank tiles at the bottom of the spritesheet. 
  this.numberOfTiles = numTiles;
  texture.wrapS = texture.wrapT = THREE.RepeatWrapping; 
  texture.repeat.set( 1 / this.tilesHorizontal, 1 / this.tilesVertical );

  // how long should each image be displayed?
  this.tileDisplayDuration = tileDispDuration;

  // how long has the current image been displayed?
  this.currentDisplayTime = 0;

  // which image is currently being displayed?
  this.currentTile = 0;

  this.update = function( milliSec )
  {
    this.currentDisplayTime += milliSec;
    while (this.currentDisplayTime > this.tileDisplayDuration)
    {
      this.currentDisplayTime -= this.tileDisplayDuration;
      this.currentTile++;
      if (this.currentTile == this.numberOfTiles)
        this.currentTile = 0;
      var currentColumn = this.currentTile % this.tilesHorizontal;
      texture.offset.x = currentColumn / this.tilesHorizontal;
      var currentRow = Math.floor( this.currentTile / this.tilesHorizontal );
      texture.offset.y = currentRow / this.tilesVertical;
    }
  };
}		

function createGrid(d){
  if(typeof d === "undefined"){d = {};}
  if(typeof d.size === "undefined"){d.size = 30;}
  if(typeof d.step === "undefined"){d.step = .2;}
  //colors and materials
  if(typeof d.material === "undefined"){d.material = "line";}
  if(typeof d.color === "undefined"){d.color = "green";}
  var geometry = new THREE.Geometry();
  var material = materials(d.material, d.color);
  for ( var i = - d.size; i <= d.size; i += d.step){
    geometry.vertices.push(new THREE.Vector3( - d.size, - 0.04, i ));
    geometry.vertices.push(new THREE.Vector3( d.size, - 0.04, i ));
    geometry.vertices.push(new THREE.Vector3( i, - 0.04, - d.size ));
    geometry.vertices.push(new THREE.Vector3( i, - 0.04, d.size ));
  }
  var grid = new THREE.Line( geometry, material, THREE.LineSegments);
  scene.add(grid);
  return grid;
}

function materials(m,c){
  if(m == "normal"){
    var material = new THREE.MeshNormalMaterial();
  }else if(m == "basic"){
    var material = new THREE.MeshBasicMaterial( {color: c} );
  }else if(m == "line"){
    var material = new THREE.LineBasicMaterial({color: c});
  }else{
    var material = new THREE.MeshNormalMaterial();
  }

  return material;
}

function onWindowResize() {
  var WIDTH = window.innerWidth,
      HEIGHT = window.innerHeight;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();

}
