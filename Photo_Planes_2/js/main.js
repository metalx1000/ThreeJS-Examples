/*-- ------------------------------------------------------------ 
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

*/

var camera, scene, renderer, controls, raycaster;
var planes = [];
var plane_size=100;
var photos = [];

var mouse = {
  x: 0,
  y: 0
},
INTERSECTED;

init();
animate();

function init() {
  scene = new THREE.Scene();

  createRenderer();
  createCamera();
  //

  //addPhoto({img:'res/family.jpg',size:200});
  getPhotos();
  window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}

function addPhoto(photo){
  var texture = new THREE.TextureLoader().load( photo.img );
  var geometry = new THREE.PlaneGeometry(photo.size, photo.size*.75);
  var material = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide } );
  var mesh = new THREE.Mesh(geometry, material);
  //scene.add(mesh);
  return mesh;
}

function animate() {
  requestAnimationFrame( animate );
  renderer.render( scene, camera );
  controls.update();
  TWEEN.update();
}

function createCamera(){
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 5000 );
  camera.position.z = 400;
  camera.position.y = 400;
  camera.lookAt(scene.position);

  //addcontrols
  controls = new THREE.OrbitControls( camera, renderer.domElement );
}

function createRenderer(){
  renderer = new THREE.WebGLRenderer( { antialias: true,alpha: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

}


function getPhotos(){
  $.get('photo_list.php',function(data){
    photos = data.split('\n');
  }).done(loadPhotos,plane_size);

  return true;
}

function loadPhotos(){
  //Shuffle Array
  photos = shuffle(photos);

  photos.forEach(function(img,i){
    var size = plane_size;
    var plane = addPhoto({img:img,size:size});
    planes.push(plane);
    plane.rotation.y = 3;
    plane.position.x = (i%10 * size) - (5 * size);
    //console.log(Math.floor(i/10));
    plane.position.y = ((Math.floor(i/10) * (size * .75)) - (Math.floor(photos.length/10)/2) * (size * .75));
    scene.add(plane);
    CLICKABLE.push(plane);
    tweenLoad(plane);
  });
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    if(temporaryValue != ""){
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  }

  return array;
}

function tweenLoad(plane){
  var time = Math.floor(Math.random() * 3000) + 1000;
  new TWEEN.Tween( plane.rotation ).to( {
    x: 0,
    y: 0,
    z: 0
  }, time )
  .easing( TWEEN.Easing.Elastic.Out).start();

  return true;
}

function tweenRandom(){
  //Select random photo
  var time = Math.floor(Math.random() * 3000) + 1000;
  var plane = planes[Math.floor(Math.random()*planes.length)];
  var rand = Math.random() * (Math.floor(Math.random()*2) == 1 ? 1 : -1);
  var tween1 = new TWEEN.Tween( plane.rotation ).to( {
    x: 0,
    y: rand,
  z: 0}, time )
  .easing( TWEEN.Easing.Elastic.Out);

  var tween2 = new TWEEN.Tween( plane.rotation ).to( {
    x: 0,
    y: 0,
  z: 0}, time )
  .easing( TWEEN.Easing.Elastic.Out);

  tween1.chain(tween2);
  tween1.start();
}

function tweenMouseOver(plane){
  //Select random photo
  var time = Math.floor(Math.random() * 3000) + 1000;
  var rand = Math.random() * (Math.floor(Math.random()*2) == 1 ? 1 : -1);
  var tween1 = new TWEEN.Tween( plane.rotation ).to( {
    x: 0,
    y: rand,
  z: 0}, time )
  .easing( TWEEN.Easing.Elastic.Out);

  var tween2 = new TWEEN.Tween( plane.rotation ).to( {
    x: 0,
    y: 0,
  z: 0}, time )
  .easing( TWEEN.Easing.Elastic.Out);

  tween1.chain(tween2);
  tween1.start();
}

//////////////////click objects                      
var CLICKED, CLICKABLE = [], CLICKGROUP = [], LASTCLICKED, CLICKEDTIME = new Date().getTime();
var raycaster = new THREE.Raycaster();  
var mouse = new THREE.Vector2();
document.addEventListener( 'mousedown', onDocumentMouseDown, false );
document.addEventListener( 'touchstart', onDocumentTouchStart, false );
function onDocumentTouchStart( event ) {
  event.preventDefault();
  event.clientX = event.touches[0].clientX;
  event.clientY = event.touches[0].clientY;
  onDocumentMouseDown( event );
}  

function onDocumentMouseDown( event ) {
 
  event.preventDefault();
  mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
  mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;
  raycaster.setFromCamera( mouse, camera );
  var intersects = raycaster.intersectObjects( CLICKABLE );

  if ( intersects.length > 0 ) {
    LASTCLICKED = CLICKED;
    CLICKEDTIME = new Date().getTime();
    CLICKED = intersects[0].object;
    //add_remove(CLICKGROUP,CLICKED);
    tweenMouseOver(CLICKED);
  }
}
//////////////////click objects end


