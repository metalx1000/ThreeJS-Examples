<!doctype html>
<html lang="en">
<head>
  <title>Cube Box #1 (Three.js)</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <style>
    canvas{
      position: absolute; 
      left:0px; 
      top:0px
    }
  </style>
</head>
<body>

<script src="libs/three.js"></script>
<script src="libs/Detector.js"></script>
<script src="libs/OrbitControls.js"></script>


<script src="js/main.js"></script>

</body>
</html>


