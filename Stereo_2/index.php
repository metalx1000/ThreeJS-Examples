<!doctype html>
<html lang="en">
<head>
  <title>Stereo 3D Cubes (Three.js)</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <style>
    canvas{
      position: absolute; 
      left:0px; 
      top:0px;
/*      background: none;*/
      background-image: linear-gradient(to bottom, red, yellow);
      z-index: 1;
    }
                        body {
                                font-family: Monospace;
                                background-color: #000;
                                color: #000;
                                margin: 0px;
                                overflow: hidden;
                        }
    #vr{
      position:fixed;
      left: 0px;
      top: 0px;
    }

  </style>
</head>
<body>

<script src="libs/three.js"></script>
<script src="libs/THREEx.FullScreen.js"></script>
<script src="libs/StereoEffect.js"></script>
<script src="libs/Detector.js"></script>
<script src="libs/OrbitControls.js"></script>
<script src="libs/DeviceOrientationControls.js"></script>


<script src="js/main.js"></script>

<div id="vr">
  <img src="icons/vr.png">
</div>
</body>
</html>


