/*-- ------------------------------------------------------------ 
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

*/

var camera, scene, renderer, stereo_renderer, motion_controls;
var mesh, light1, ground;
var speed=5;
var forward=false;
var stereo = true;
var pos = [];
init();
animate();

function init() {
  scene = new THREE.Scene();
  addCamera();
  addRenderer();

  addControls();

  //Fullscreen on click
  window.addEventListener( 'click', fullscreen, false);
  document.body.addEventListener('touchend', fullscreen, false);

  //movements
  window.addEventListener( 'touchstart', function(){forward=true}, false);
  window.addEventListener( 'touchend', function(){forward=false}, false);

  window.addEventListener( 'resize', onWindowResize, false );

  light1 = addLight();
  ADD = setInterval(addCube,50);

  ground = addGround();
}

function fullscreen(){
  if( THREEx.FullScreen.available() ){
    THREEx.FullScreen.request();
  }
}

function addCamera(){
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 2000 );
  camera.position.z = -400;
  camera.position.y = 200;
  camera.lookAt(new THREE.Vector3(0,1000,0));

}

function addRenderer(){

  renderer = new THREE.WebGLRenderer( { antialias: true,alpha: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );

  renderer.setClearColor(0x000000, 0.0);

  //Stereo Effect
  if ( stereo ){
    stereo_renderer = new THREE.StereoEffect( renderer );
    stereo_renderer.setSize( window.innerWidth, window.innerHeight );
  }

  document.body.appendChild( renderer.domElement );

}

function addCube(){
  var grid = 5;
  var size = 50;
  var offset = (size * grid)/2;

  //check if all spots have been filled
  if(pos.length >= grid*grid*grid){
    clearInterval(ADD);
    //alert("All Positions Filled");
    return false;
  }

  var texture = new THREE.TextureLoader().load( 'textures/crate.gif' );
  var geometry = new THREE.BoxBufferGeometry( size, size, size );
  var material = new THREE.MeshBasicMaterial( { map: texture } );

  mesh = new THREE.Mesh( geometry, material );
  mesh.size = size;

  var p = Math.floor((Math.random() * grid * grid * grid));

  //check if position has already been used
  if(pos.includes(p)){
    addCube();
  }else{
    pos.push(p);

    var row = (p%grid) *size;
    var vert = Math.floor((p%(grid*grid))/grid)*size;
    //console.log(p);
    var col = Math.floor(p/grid/grid)*size;
    //console.log(row + " : " + vert + " : " + col);
    mesh.position.set(row - offset,vert - offset + 150, col - offset - 100);
    scene.add( mesh );
    return mesh;
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  //renderer.setSize( window.innerWidth, window.innerHeight );
  if (stereo){
    stereo_renderer.setSize( window.innerWidth, window.innerHeight );
  }else{
    renderer.setSize( window.innerWidth, window.innerHeight );
  }
}

function animate() {
  requestAnimationFrame( animate );
  //renderer.render( scene, camera );
  motion_controls.update();
  if(stereo){
    stereo_renderer.render( scene, camera );
  }else{
    renderer.render(scene,camera);
  }

  if(forward){
    camera.translateZ( - speed );
  }
}

function addControls(){
  //Mouse/Touch controls
/*
  var controls = new THREE.OrbitControls( camera, renderer.domElement );
  controls.maxPolarAngle = Math.PI * 0.5;
  controls.minDistance = 100;
  controls.maxDistance = 1000;
  //Device accelerometer/motion controls
*/  
  motion_controls = new THREE.DeviceOrientationControls( camera, renderer.domElement );

}


function addGround(){

  var texture = new THREE.TextureLoader().load( 'textures/crate.gif' );
  var geometry = new THREE.BoxBufferGeometry( 1000, 1, 1000 );
  var material = new THREE.MeshBasicMaterial( { map: texture } );

  mesh = new THREE.Mesh( geometry, material );
  //mesh.position.y = -200;
  scene.add(mesh);

  return mesh;
}


function addLight(){
  // create a point light
  var pointLight = new THREE.PointLight(0xFFFFFF);

  // set its position
  pointLight.position.x = 10;
  pointLight.position.y = 50;
  pointLight.position.z = 130;

  // add to the scene
  scene.add(pointLight);

  return pointLight;
}

